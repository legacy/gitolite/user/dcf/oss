/*
Usage: http://localhost/scan?url=http://goo.gl/qwJgW

http://goo.gl/qwJgW → 301 Moved Permanently
http://shore10.me/799657 → 301 Moved Permanently
http://bit.ly/SI6nqJ → 301 Moved
http://ur1.ca/gja → 302 Found
http://example.com/ → 302 Found
http://www.iana.org/domains/example/ → 200 OK
*/

package main

import "bytes"
import "errors"
import "fmt"
import "html"
import "io"
import "io/ioutil"
import "net/http"
import "net/http/cgi"
import "os"

const maxBodySize = 10 * 1024 * 1024

func exitError(code int, msg string) {
	fmt.Printf("Status: %d\r\n", code)
	fmt.Printf("Content-Type: text/plain; charset=utf-8\r\n")
	fmt.Printf("\r\n")
	fmt.Printf("Error: %s.\n", msg)
	os.Exit(1)
}

func getFirstParam(req *http.Request, param string) (value string, err error) {
	values, ok := req.URL.Query()[param]
	if !ok || len(values) < 1 {
		err = errors.New("no \"%s\" parameter")
		return
	}

	return values[0], nil
}

func doURL(method string, url string, body []byte) {
	const MAX_REQUESTS = 100
	var prev *http.Request
	var n int

	n = 0
redirects:
	for {
		fmt.Printf("%s %s", method, html.EscapeString(url))

		if n >= MAX_REQUESTS {
			fmt.Printf(" &rarr; reached limit of %d requests\n", MAX_REQUESTS)
			break
		}
		n++

		req, err := http.NewRequest(method, url, bytes.NewReader(body))
		if err != nil {
			fmt.Printf(" &rarr; error: %s\n", html.EscapeString(err.Error()))
			break
		}
		if prev != nil && prev.URL.Scheme == req.URL.Scheme {
			req.Header.Set("Referer", prev.URL.Scheme)
		}
		resp, err := http.DefaultTransport.RoundTrip(req)
		if err != nil {
			fmt.Printf(" &rarr; error: %s\n", html.EscapeString(err.Error()))
			break
		}
		fmt.Printf(" &rarr; %s\n", html.EscapeString(resp.Status))

		/* Deliberately not reading the body here. */
		switch resp.StatusCode {
		case http.StatusMovedPermanently,
			http.StatusFound,
			http.StatusSeeOther:
			/* Convert POST to GET; see RFC 2616 sections 10.3.2–4. */
			if method != "HEAD" {
				method = "GET"
			}
			body = make([]byte, 0)
		case http.StatusTemporaryRedirect:
			/* Keep POST as POST; see RFC 2616 section 10.3.8. */
		default:
			/* No more redirects. */
			break redirects
		}

		url = resp.Header.Get("Location")
		if url == "" {
			fmt.Print("Missing Location header field")
			break
		}

		prev = req
	}
}

func main() {
	req, err := cgi.Request()
	if err != nil {
		exitError(http.StatusInternalServerError, err.Error())
	}
	switch req.Method {
	case "GET", "HEAD", "POST":

	default:
		exitError(http.StatusMethodNotAllowed, "request method must be GET, HEAD, or POST")
	}

	fmt.Printf("Status: 200\r\n")
	fmt.Printf("Content-Type: text/html; charset=utf-8\r\n")
	fmt.Printf("Connection: close\r\n")
	fmt.Printf("\r\n")
	fmt.Printf(`<!DOCTYPE html>
<html>
<head>
<title>online scanning service</title>
</head>
<body>
<h3>online scanning service</h3>
`)

	fmt.Printf(`<p>
Your request: <code>%s %s %s</code><br>
`, html.EscapeString(req.Method), html.EscapeString(req.URL.RequestURI()), html.EscapeString(req.Proto))
	if req.ContentLength < 0 {
		fmt.Printf("With unknown Content-Length\n")
	} else if req.ContentLength == 0 {
		fmt.Printf("With no request body\n")
	} else if req.ContentLength > 0 {
		fmt.Printf("With Content-Length of %d bytes\n", req.ContentLength)
	}

	fmt.Printf("<br>\n")

	var body []byte
	if req.Body != nil {
		body, err = ioutil.ReadAll(io.LimitReader(req.Body, maxBodySize))
		if err != nil {
			fmt.Printf("Error reading body: %s", html.EscapeString(err.Error()))
		} else {
			fmt.Printf("Read %d bytes of body\n", len(body))
		}
	} else {
		body = make([]byte, 0)
	}

	fmt.Printf("</p>\n")

	url, err := getFirstParam(req, "url")
	if err == nil {
		fmt.Printf("<pre>\n")
		doURL(req.Method, url, body)
		fmt.Printf("</pre>\n")
		fmt.Printf("<hr>\n")
	}

	fmt.Print(`<p>
Pass the <code>url</code> parameter to give a URL to scan.
Demo: <a href="?url=http://goo.gl/qwJgW">?url=http://goo.gl/qwJgW</a>
</p>
<form method=GET>
<p>
<label for=url><code>url=</code></label><input name=url placeholder="Enter URL"></input>
</p>
<input type=submit>
</form>
</body>
<p>
If you make a request using GET, this program will also use GET. If you make a
request using POST, this program will also use POST.
</p>
</html>
`)
}
