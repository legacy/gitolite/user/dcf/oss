import base64
import BaseHTTPServer
import cgi
import os
import re
import socket
import sys
import time
import urllib
import urlparse

# How much data we reckon we can stuff in a URL.
GET_CHUNK_SIZE = 256
POST_CHUNK_SIZE = 256000		

DEBUG = False
def setdebug(val):
    global DEBUG
    DEBUG = val
def logdebug(msg):
    if DEBUG:
        sys.stderr.write(str(time.time()) + ": " + msg + "\n")

class Buffer(object):
    def __init__(self):
        self.buf = ""
        self.will_shutdown = False
        self.is_shutdown = False
        self.fin_sent = False
    def push(self, data):
        assert len(data) == 0 or not (self.will_shutdown or self.is_shutdown)
        self.buf += data
    def advance(self, n):
        self.buf = self.buf[n:]
    def shutdown(self):
        self.will_shutdown = True
        self.is_shutdown = True
    def __getslice__(self, i, j):
        return self.buf[i:j]
    def __len__(self):
        return len(self.buf)

class Stream(object):
    def __init__(self, s, id):
        # Socket.
        self.s = s
        self.id = id
        self.seq = 0
        self.ack = 0
        # Number of bytes sent but not acknowledged.
        self.unacked = 0
        self.last_send_time = None
        self.refresh_interval = 100.0
        # Buffer of bytes to be forwarded from the local socket over the
        # redirects transport.
        self.rbuf = Buffer()
        # Buffer of bytes received from the redirects transport to be forwarded
        # to the local socket.
        self.wbuf = Buffer()
        # The URL which redirects point to.
        self.peer_url = None
        self.redirect_class = None
        # Maximum allowed payload length
        self.chunk_size = None
    def fileno(self):
        return self.s.fileno()
    def __str__(self):
        try:
            host, port = socket.getnameinfo(self.s.getsockname(), socket.NI_NUMERICHOST | socket.NI_NUMERICSERV)
            return "%s:%s:%s" % (host, port, self.id)
        except AttributeError:
            return self.id

def format_netloc(addr):
    host, port = addr
    try:
        addrs = socket.getaddrinfo(host, port, 0, socket.SOCK_STREAM, socket.IPPROTO_TCP, socket.AI_NUMERICHOST)
    except socket.gaierror:
        # Not a numeric address.
        return "%s:%d" % (host, port)
    af = addrs[0][0]
    if af == socket.AF_INET:
        return "%s:%d" % (host, port)
    elif af == socket.AF_INET6:
        return "[%s]:%d" % (host, port)
    else:
        raise ValueError("Unknown address family %d" % af)

def url_path_components(path):
    """Returns the components of an absolute URL path."""
    path = re.sub(r'/+', "/", path)
    if path.startswith("/"):
        path = path[1:]
    if path.endswith("/"):
        path = path[:-1]
    if path == "":
        return []
    return re.split(r'/', path)

def make_query_string(query):
    query_parts = []
    for k, v in query:
        if v == False:
            continue
        elif v == True:
            query_parts.append(k)
        else:
            query_parts.append(urllib.urlencode(((k, v),)))
    return "&".join(query_parts)

def gen_nonce():
    return os.urandom(4).encode("hex")

def make_stream_url(stream, query):
    """Get a URL that opens a new stream or refers to an existing stream on the
    relay's server transport plugin. This is the URL that should be submitted to
    a scanning service that follows redirects."""
    parts = list(urlparse.urlparse(stream.peer_url))
    nonce = gen_nonce()
    parts[2] = "/" + nonce + "/" + stream.id + "/" + str(stream.seq) + "/" + str(stream.ack) + "/" + str(stream.redirect_class.name) + "/" + str(stream.chunk_size)
    parts[3] = ""
    parts[4] = make_query_string(query)
    parts[5] = ""
    return urlparse.urlunparse(parts)

class StreamHandler(BaseHTTPServer.BaseHTTPRequestHandler):
#     def do_HEAD(self): #This is needed for OSSes that sends their initial scan as a HEAD request (e.g. novirusthanks)
#         logdebug("Got HEAD request. Processing as a GET request.")
#         self.do_GET()
#         return
    def do_GET(self):
        logdebug("Request from %s:" % format_sockaddr(self.client_address))
        logdebug("  %s %s %s" % ("GET", self.path, self.request_version))
        logdebug("\n".join("  " + line for line in str(self.headers).splitlines()))

        parts = urlparse.urlparse(self.path)
        path = parts[2]

        if path == "/robots.txt":
            self.send_response(200)
            self.send_header("Content-Type", "text/plain")
            self.end_headers()
            self.wfile.write("""\
User-agent: *
Disallow:
""")
            return

        components = url_path_components(path)
        try:
            nonce, stream_id, seq, ack = components[:4]
            seq = int(seq)
            ack = int(ack)
        except ValueError:
            self.send_error(400)
            return

        logdebug("got GET request with stream_id %r seq %d ack %d" % (stream_id, seq, ack))

        return self.do_stream_request(stream_id, seq, ack)

    def do_POST(self):
        logdebug("Request from %s:" % format_sockaddr(self.client_address))
        logdebug("  %s %s %s" % ("POST", self.path, self.request_version))
        logdebug("\n".join("  " + line for line in str(self.headers).splitlines()))

        parts = urlparse.urlparse(self.path)
        path = parts[2]
        components = url_path_components(path)
        try:
            nonce, stream_id, seq, ack, method, chunk_size = components[:6]
            seq = int(seq)
            ack = int(ack)
            chunk_size = int(chunk_size)
        except ValueError:
            self.send_error(400)
            return

        logdebug("got POST request with stream_id %r seq %d ack %d" % (stream_id, seq, ack))

        return self.do_stream_request(stream_id, seq, ack)

    def get_first_param(self, name):
        parts = urlparse.urlparse(self.path)
        for k, v in cgi.parse_qsl(parts[4], keep_blank_values=True):
            if k == name:
                return v
        return None

    def extract_payload_raw(self):
        url_data = self.get_first_param("data")
        if url_data is not None:
            return url_data
        if self.command == "POST":
            content_type = self.headers.getheader("content-type")
            content_length = self.headers.getheader('content-length')
            if content_length is None:
                return None
            content_length = int(content_length)
            body = self.rfile.read(content_length)
            if "application/x-www-form-urlencoded" in content_type:
                qs = cgi.parse_qs(body, keep_blank_values=True)
                return qs["data"][0]
            else:
                return body
        else:
            return None

    def extract_payload(self):
        return base64.urlsafe_b64decode(self.extract_payload_raw() or "")

    def extract_fin(self):
        """Get the value of the fin flag, whether the stream is to be considered
        closed after this."""
        return self.get_first_param("fin") is not None

    def extract_method_name(self):
        parts = urlparse.urlparse(self.path)
        path = parts[2]
        components = url_path_components(path)
        return components[4]

    def extract_chunk_size(self):
        parts = urlparse.urlparse(self.path)
        path = parts[2]
        components = url_path_components(path)
        return int(components[5])

    def extract_return_url(self):
        return self.get_first_param("return")

    def redirect_len(self, redirect_fn, redirect_class, stream, n):
        peer_url = make_stream_url(stream, "") + "?data="
        if redirect_class.type == "GET":
            n -= len(peer_url)
        peer_url += "X" * n
        logdebug("stream %s redirecting to %s" % (stream, peer_url))
        redirect_fn(self, peer_url)
        return max(n, 0)

    def _redirect_generic(self, redirect_fn, stream, query):
        peer_url = make_stream_url(stream, query)
        logdebug("stream %s redirecting to %s" % (stream, peer_url))
        redirect_fn(self, peer_url)
        stream.last_send_time = time.time()

    def redirect_fin(self, redirect_fn, stream, data=""):
        query = [("fin", True),]
        if data:
            query.append(("data", base64.urlsafe_b64encode(data)))
        return self._redirect_generic(redirect_fn, stream, query)

    def redirect_data(self, redirect_fn, stream, data):
        query = []
        if data:
            query.append(("data", base64.urlsafe_b64encode(data)))
        return self._redirect_generic(redirect_fn, stream, query)

    # Don't do reverse DNS lookup for logging purposes.
    def address_string(self):
        return format_sockaddr(self.client_address)

    def log_request(self, *args, **kwargs):
        if DEBUG:
            BaseHTTPServer.BaseHTTPRequestHandler.log_request(self, *args, **kwargs)

    def log_error(self, *args, **kwargs):
        if DEBUG:
            BaseHTTPServer.BaseHTTPRequestHandler.log_error(self, *args, **kwargs)

def parse_addr_spec(spec, defhost = None, defport = None):
    host = None
    port = None
    af = 0
    m = None
    # IPv6 syntax.
    if not m:
        m = re.match(ur'^\[(.+)\]:(\d*)$', spec)
        if m:
            host, port = m.groups()
            af = socket.AF_INET6
    if not m:
        m = re.match(ur'^\[(.+)\]$', spec)
        if m:
            host, = m.groups()
            af = socket.AF_INET6
    # IPv4/hostname/port-only syntax.
    if not m:
        try:
            host, port = spec.split(":", 1)
        except ValueError:
            host = spec
        if re.match(ur'^[\d.]+$', host):
            af = socket.AF_INET
        else:
            af = 0
    host = host or defhost
    port = port or defport
    if port is not None:
        port = int(port)
    return host, port

def format_addr(addr):
    host, port = addr
    if not host:
        return u":%d" % port
    # Numeric IPv6 address?
    try:
        addrs = socket.getaddrinfo(host, port, 0, socket.SOCK_STREAM, socket.IPPROTO_TCP, socket.AI_NUMERICHOST)
        af = addrs[0][0]
    except socket.gaierror, e:
        af = 0
    if af == socket.AF_INET6:
        result = u"[%s]" % host
    else:
        result = "%s" % host
    if port is not None:
        result += u":%d" % port
    return result

def format_sockaddr(sockaddr):
    host, port = socket.getnameinfo(sockaddr, socket.NI_NUMERICHOST | socket.NI_NUMERICSERV)
    port = int(port)
    return format_addr((host, port))

def guess_external_ip():
    ai = socket.getaddrinfo("8.8.8.8", 0, socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)[0]
    s = socket.socket(ai[0], ai[1], ai[2])
    s.connect(ai[4])
    sockaddr = s.getsockname()
    return sockaddr[0]

def parse_oss_spec(spec, defoss=None, defmethod=None):
    try:
        oss_name, method_name = spec.split("/", 1)
    except ValueError:
        oss_name, method_name = spec, None
    return (oss_name or defoss, method_name or defmethod)

