#!/usr/bin/env python

import getopt
import sys
import urllib2

def usage():
    print """\
Usage: %s [OPTIONS]
Prints out your public IP address by querying a public service.
  -4  get IPv4 address
  -6  get IPv6 address
  -h, --help  show this help\
""" % sys.argv[0]

url = "https://wtfismyip.com/text"

opts, args = getopt.gnu_getopt(sys.argv[1:], "46h", ["help"])
for o, a in opts:
    if o == "-4":
        url = "https://ipv4.wtfismyip.com/text"
    elif o == "-6":
        url = "https://ipv6.wtfismyip.com/text"
    elif o == "-h" or o == "--help":
        usage()
        sys.exit()
        
ip = urllib2.urlopen(url).read().strip()
if ":" in ip:
    print "[" + ip + "]"
else:
    print ip
