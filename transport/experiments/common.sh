# This file contains common variables and subroutines used by the experiment
# scripts.

REDIRECTS_DIR="$(dirname $BASH_SOURCE)/.."

SOCAT=socat
TOR=tor

visible_sleep() {
	N="$1"
	echo -n "sleep $N"
	while [ "$N" -gt 0 ]; do
		sleep 1
		N=$((N-1))
		echo -ne "\rsleep $N "
	done
	echo -ne "\n"
}


# Run a command and get the "real" part of time(1) output as a number of
# seconds.
real_time() {
	# Make a spare copy of stderr (fd 1).
	exec 3>&2
	# Point the subcommand's stderr to our copy (fd 3), and extract the
	# original stderr (fd 2) output of time.
	(time -p eval "$@" 2>&3) 2>&1 | tail -n 3 | head -n 1 | awk '{print $2}'
}

# Repeat a subcommand N times.
repeat() {
	local N
	N="$1"
	shift
	while [ $N -gt 0 ]; do
		eval "$@"
		N=$((N-1))
	done
}
