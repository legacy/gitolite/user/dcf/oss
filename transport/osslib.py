import cgi
import urllib2
import cookielib

from threading import Thread

from common import *

#this is needed for OSSGoogleShort
from HTMLParser import HTMLParser

#from poster.encode import multipart_encode
import httplib, mimetypes

def make_return_url(addr):
    netloc = format_netloc(addr)
    parts = ["http", netloc, "", "", "", ""]
    return urlparse.urlunparse(parts)

def readall(f):
    while True:
        d = f.read(1024)
        if not d:
            break

class OSS(object):
    def initiate(self, stream, my_addr):
        thread = Thread(target=self._initiate, args=(stream, my_addr))
        thread.setDaemon(True)
        thread.start()

    def make_init_url(self, stream, my_addr):
        return_url = make_return_url(my_addr)
        stream.chunk_size = self.get_max_allowed_chunk_size(stream.redirect_class)
        return make_stream_url(stream, (("return", return_url),))

    def logged_req(self, req):
        logdebug("%s: initiate" % self.name)
        logdebug("  %s %s" % (req.get_method(), req.get_full_url()))
        for name, value in req.header_items():
            logdebug("  %s: %s" % (name, value))
        if req.has_data():
            logdebug("  + %d bytes of data" % len(str(req.get_data())))
        try:
            readall(urllib2.urlopen(req))
        except Exception, e:
            logdebug("Exception in urlopen: %s" % str(e))

    def get_max_allowed_chunk_size(self, method):
        if method.type == "POST":
            return self.POST_CHUNK_SIZE
        else:
            return self.GET_CHUNK_SIZE


class OSSManual(OSS):
    """The simplest OSS, that just prints a URL to stderr so you can load it in
    a browser."""
    GET_CHUNK_SIZE = 256
    POST_CHUNK_SIZE = 256000		
    name = "manual"
    description = "print URL to stderr"
    def _initiate(self, stream, my_addr):
        print >> sys.stderr, "Manual URL:"
        print >> sys.stderr, self.make_init_url(stream, my_addr)

class OSSReference(OSS):
    BASE_URL = "http://redirects.bamsoftware.com/scan"
    GET_CHUNK_SIZE = 8192
    POST_CHUNK_SIZE = 256000		
    name = "ref"
    description = "reference scanner"
    def _initiate(self, stream, my_addr):
        parts = list(urlparse.urlparse(OSSReference.BASE_URL))
        parts[4] = urllib.urlencode((("url", self.make_init_url(stream, my_addr)),))
        scan_url = urlparse.urlunparse(parts)

        req = urllib2.Request(scan_url)
        self.logged_req(req)

class OSSVirusTotal(OSS):
    HOMEPAGE_URL = "https://www.virustotal.com/"
    SUBMISSION_URL = "https://www.virustotal.com/url/submission/"
    GET_CHUNK_SIZE = 1400
    POST_CHUNK_SIZE = 256000		
    name = "virustotal"
    description = "VirusTotal URL scanner"
    def _initiate(self, stream, my_addr):
        # Retrieve the home page to get a CSRF token.
        homepage = urllib2.urlopen(OSSVirusTotal.HOMEPAGE_URL)
        csrfcookie = homepage.info()["Set-Cookie"]
        m = re.match(r'\bcsrftoken=(\w+);', csrfcookie)
        csrftoken = m.group(1)

        body = urllib.urlencode((("url", self.make_init_url(stream, my_addr)),))
        headers = dict((
            ("Referer", OSSVirusTotal.HOMEPAGE_URL),
            ("Cookie", "csrftoken=%s" % csrftoken),
            ("X-CSRFToken", csrftoken),
            ("X-Requested-With", "XMLHttpRequest"),
        ))

        req = urllib2.Request(OSSVirusTotal.SUBMISSION_URL, body, headers)
        self.logged_req(req)

class OSSAdSense(OSS):
    BASE_URL = "https://googleads.g.doubleclick.net/pagead/ads?client=ca-nytimes_homepage_js&output=js"
    GET_CHUNK_SIZE = 1024
    POST_CHUNK_SIZE = 256000		
    name = "adsense"
    description = "AdSense MediaBot scanner"
    def _initiate(self, stream, my_addr):
        parts = list(urlparse.urlparse(OSSAdSense.BASE_URL))
        qsl = list(cgi.parse_qsl(parts[4]))
        qsl.append(("url", self.make_init_url(stream, my_addr)))
        parts[4] = urllib.urlencode(qsl)
        scan_url = urlparse.urlunparse(parts)

        req = urllib2.Request(scan_url)
        self.logged_req(req)

class OSSPdfMyUrl(OSS):
    BASE_URL = "http://pdfmyurl.com/"
    GET_CHUNK_SIZE = 512000
    POST_CHUNK_SIZE = 512000		
    name = "pdfmyurl"
    description = "web page PDF converter"
    def _initiate(self, stream, my_addr):
        values = { 'url' : self.make_init_url(stream, my_addr), 'Submit.x' : '0', 'Submit.y' : '0', 'Submit' : 'Submit' }
        data = urllib.urlencode(values)

        req = urllib2.Request(OSSPdfMyUrl.BASE_URL, data)
        self.logged_req(req)

class OSSvURLDissect(OSS):
    BASE_URL = "http://vurldissect.co.uk/default.asp?btnvURL=Dissect&selUAStr=1&selServer=1&ref=&cbxSource=on&cbxBlacklist=on"
    GET_CHUNK_SIZE = 32768
    POST_CHUNK_SIZE = 256000		
    name = "vurldissect"
    description = "web page dissection service"
    def _initiate(self, stream, my_addr):
        parts = list(urlparse.urlparse(OSSvURLDissect.BASE_URL))
        qsl = list(cgi.parse_qsl(parts[4]))
        qsl.insert(0,("url", self.make_init_url(stream, my_addr))) # add the url parameter at the beginning.
        parts[4] = urllib.urlencode(qsl)
        scan_url = urlparse.urlunparse(parts)

        req = urllib2.Request(scan_url)
        self.logged_req(req)

class OSSW3C(OSS):
    BASE_URL = "http://validator.w3.org/check"
    GET_CHUNK_SIZE = 4096
    POST_CHUNK_SIZE = 256000		
    name = "w3c"
    description = "W3C markup validation service"
    def _initiate(self, stream, my_addr):
        parts = list(urlparse.urlparse(OSSW3C.BASE_URL))
        qsl = list(cgi.parse_qsl(parts[4]))
        qsl.append(("uri", self.make_init_url(stream, my_addr)))
        parts[4] = urllib.urlencode(qsl)
        scan_url = urlparse.urlunparse(parts)

        req = urllib2.Request(scan_url)
        self.logged_req(req)

class OSSGOMO(OSS):
    BASE_URL = "http://www.howtogomo.com/en/d/gomometer"
    GET_CHUNK_SIZE = 32768
    POST_CHUNK_SIZE = 512000		
    description = "GoMoMeter mobile browser preview"
    name = "gomo"
    def _initiate(self, stream, my_addr):
        parts = list(urlparse.urlparse(OSSGOMO.BASE_URL))
        qsl = list(cgi.parse_qsl(parts[4]))
        qsl.append(("url", self.make_init_url(stream, my_addr)))
        parts[4] = urllib.urlencode(qsl)
        scan_url = urlparse.urlunparse(parts)

        req = urllib2.Request(scan_url)
        self.logged_req(req)

class OSSNetRenderer(OSS):
    BASE_URL = "http://netrenderer.com/index.php"
    GET_CHUNK_SIZE = 1024
    POST_CHUNK_SIZE = 256000		
    name = "netrenderer"
    description = "IE NetRenderer Internet Explorer renderer"
    def _initiate(self, stream, my_addr):
        values = { 'browser' : 'ie7', 'url' : self.make_init_url(stream, my_addr), 'voffset' : '0', 'go' : 'Render' }
        data = urllib.urlencode(values)

        req = urllib2.Request(OSSNetRenderer.BASE_URL, data)
        self.logged_req(req)

#Note: the user must be logged into Google for this code to work
class OSSGoogleShort(OSS):
    LOGIN_URL = "https://accounts.google.com/ServiceLoginAuth"
    HOMEPAGE_URL = "http://goo.gl/"
    SUBMISSION_URL = "http://goo.gl/api/shorten"
    GET_CHUNK_SIZE = 1024
    POST_CHUNK_SIZE = 512000		
    name = "goo.gl"
    description = "goo.gl URL shortening service"
    def _initiate(self, stream, my_addr):
		#loging to Google
        cj = cookielib.CookieJar()
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
        values = { 'signIn' : 'Sign in', 'service' : 'urlshortener', 'rmShown' : '1', 'pstMsg' : '1', 'PersistentCookie' : 'yes', 'Passwd' : 'redirects', 'GALX' : 'cxxMKC6UEHE', 'followup' : 'http://goo.gl/?authed=1', 'Email' : 'redirectsmail@yahoo.com', 'dsh' : '-3744598034440683044', 'continue' : 'http://goo.gl/?authed=1' }
        login_data = urllib.urlencode(values)
        opener.open(OSSGoogleShort.LOGIN_URL, login_data)
        # Retrieve the home page to get a CSRF token.
        homepage = opener.open(OSSGoogleShort.HOMEPAGE_URL)
        parser = GoogHTMLParser()
        #len = int(homepage.info().getheaders("Content-Length")[0])
        #print homepage.read(len)
        #parser.feed(homepage.read(len))
        str = homepage.read(10000)
        print str
        parser.feed(str)

        values = { 'url' : self.make_init_url(stream, my_addr), 'captcha_challenge' : '', 'captcha_response' : '', 'security_token' : parser.sec_token }
        body = urllib.urlencode(values)
        post_response = dict(opener.open(OSSGoogleShort.SUBMISSION_URL, body))
		
        req = urllib2.Request(post_response.get("preview_url"))
        self.logged_req(req)

#this is needed for OSSGoogleShort
class GoogHTMLParser(HTMLParser):
    sec_token = None
    def handle_starttag(self, tag, attrs):
        if tag == "input" and has_attr(attrs, "id", "security_token"):
            sec_token = get_attr(attrs, "value")
            print "found sec token" + sec_token
	
    def has_attr(attrs, name, value):
        for k, v in attrs:
            if k == name and v==value:
                return True
        return False

    def get_attr(attrs, name):
        for k, v in attrs:
            if k == name:
                return v
        return None

class OSSNoVirusThanks(OSS):
    BASE_URL = "http://vscan.novirusthanks.org/?submiturl=Submit+Address"
    GET_CHUNK_SIZE = 32768
    POST_CHUNK_SIZE = 256000		
    name = "novirusthanks"
    description = "malicious website scanner"
    def _initiate(self, stream, my_addr):
        parts = list(urlparse.urlparse(OSSNoVirusThanks.BASE_URL))
        qsl = list(cgi.parse_qsl(parts[4]))
        qsl.insert(0,("url", self.make_init_url(stream, my_addr))) # add the url parameter at the beginning.
        parts[4] = urllib.urlencode(qsl)
        scan_url = urlparse.urlunparse(parts)

        req = urllib2.Request(scan_url)
        self.logged_req(req)

class OSSDrWeb(OSS):
    BASE_URL = "http://online.us.drweb.com/result/"
    GET_CHUNK_SIZE = 4096
    POST_CHUNK_SIZE = 256000
    name = "drweb"
    description = "scan for malicious websites"
    def _initiate(self, stream, my_addr):
        values = { 'lng' : 'en', 'url' : self.make_init_url(stream, my_addr), 'x' : '49', 'y' : '10' }
        data = urllib.urlencode(values)
        headers = { 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4',\
                    'Origin' : 'https://support.drweb.com', 'Accept-Encoding' : 'gzip,deflate,sdch', 'Accept-Language' : 'en-US,en;q=0.8',\
                    'Accept-Charset' : 'ISO-8859-1,utf-8;q=0.7,*;q=0.3'  }

        req = urllib2.Request(OSSDrWeb.BASE_URL, data, headers)
        self.logged_req(req)

class OSSWepawet(OSS):
    BASE_HOST = "wepawet.iseclab.org"
    BASE_PATH = "/index.php"
    GET_CHUNK_SIZE = 256
    POST_CHUNK_SIZE = 256000		
    name = "wepawet"
    description = "scan for malicious websites"
    def _initiate(self, stream, my_addr):
        values = ( ('url',self.make_init_url(stream, my_addr)), ('cmd', 'upload'))
        reply = self.post_multipart(OSSWepawet.BASE_HOST, OSSWepawet.BASE_PATH, values)

        readall(reply)

    def post_multipart(self, host, selector, fields):
        content_type, body = self.encode_multipart_formdata(fields)
        h = httplib.HTTPConnection(host)
        h.putrequest('POST', selector)
        h.putheader('content-type', content_type)
        h.putheader('content-length', str(len(body)))
        h.endheaders()
        h.send(body)

        #The following is to preserve consiste logging with all other OSSes that use the urllib2 request class.
        logdebug("%s: initiate" % self.name)
        logdebug("  %s %s" % ("POST", host + selector))
        logdebug("  %s: %s" % ("content-type", content_type))
        logdebug("  %s: %s" % ("content-length", str(len(body))))
        logdebug("  + %d bytes of data" % len(body))
        time.sleep(10)
        print h.getresponse().msg
        print h.getresponse().read(1000)

        return h.getresponse()

    def encode_multipart_formdata(self, fields):
        BOUNDARY = '----------ThIs_Is_tHe_bouNdaRY_$'
        CRLF = '\r\n'
        L = []
        for (key, value) in fields:
            L.append('--' + BOUNDARY)
            L.append('Content-Disposition: form-data; name="%s"' % key)
            L.append('')
            L.append(value)
        L.append('--' + BOUNDARY + '--')
        L.append('')
        body = CRLF.join(L)
        content_type = 'multipart/form-data; boundary=%s' % BOUNDARY
        return content_type, body


OSS_MAP = dict((x.name, x) for x in (
    OSSManual,
    OSSReference,
    OSSVirusTotal,
    OSSAdSense,
    OSSPdfMyUrl,
    OSSvURLDissect,
    OSSW3C,
    OSSGOMO,
    OSSNetRenderer,
    OSSGoogleShort,
    OSSNoVirusThanks,
    OSSDrWeb,
    OSSWepawet,
))

try:
    # http://pypi.python.org/pypi/twitter/
    # easy_install twitter
    from twitter import *
except ImportError:
    pass
else:
    class OSSTwitter(OSS):
        name = "twitter"
        description = "t.co link scanner (posts to Twitter)"
        GET_CHUNK_SIZE = 2048
        POST_CHUNK_SIZE = 256000		
        # https://dev.twitter.com/docs/auth/tokens-devtwittercom
        TOKEN = ""
        TOKEN_KEY = ""
        CON_SEC = ""
        CON_SEC_KEY = ""

        def _initiate(self, stream, my_addr):
            my_auth = OAuth(OSSTwitter.TOKEN, OSSTwitter.TOKEN_KEY, OSSTwitter.CON_SEC, OSSTwitter.CON_SEC_KEY)
            twit = Twitter(auth=my_auth)
            twit.statuses.update(status=self.make_init_url(stream, my_addr))

    OSS_MAP[OSSTwitter.name] = OSSTwitter

	
try:
    # https://github.com/facebook/fbconsole
    # easy_install fbconsole
    import fbconsolered as F
except ImportError:
    pass
else:
    class OSSFacebook(OSS):
        name = "facebook"
        description = "Facebook link scanner (posts to Facebook)"
        GET_CHUNK_SIZE = 256
        POST_CHUNK_SIZE = 256000		

        def _initiate(self, stream, my_addr):
            F.AUTH_SCOPE = ['publish_stream']
            F.authenticate()
            F.post('/me/feed', {'message': self.make_init_url(stream, my_addr)})

    OSS_MAP[OSSFacebook.name] = OSSFacebook
