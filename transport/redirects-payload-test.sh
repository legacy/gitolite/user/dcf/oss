#!/bin/bash

#    pdfmyurl/301 pdfmyurl/302 pdfmyurl/303 pdfmyurl/307 \
#    pdfmyurl/frameset pdfmyurl/iframe \
#    pdfmyurl/metarefresh pdfmyurl/refreshheader \
#    pdfmyurl/onload pdfmyurl/xhr \
OSS_METHOD_LIST="\
    adsense/301 adsense/302 adsense/303 adsense/307 \
    adsense/metarefresh adsense/refreshheader \
    gomo/300 gomo/301 gomo/302 gomo/303 gomo/307 \
    gomo/frameset gomo/iframe \
    gomo/metarefresh gomo/refreshheader \
    gomo/onload gomo/xhr \
    netrenderer/xhr netrenderer/300 netrenderer/301 netrenderer/302 netrenderer/303 netrenderer/307 \
    netrenderer/frameset netrenderer/iframe \
    netrenderer/metarefresh netrenderer/refreshheader \
    netrenderer/onload \
    novirusthanks/300 novirusthanks/301 novirusthanks/302 novirusthanks/303 novirusthanks/307 \
    virustotal/300 virustotal/301 virustotal/302 virustotal/303 virustotal/307 \
    virustotal/frameset virustotal/iframe \
    virustotal/metarefresh virustotal/refreshheader \
    virustotal/onload virustotal/xhr \
    vurldissect/300 vurldissect/301 vurldissect/302 vurldissect/303 vurldissect/307 \
    w3c/301 w3c/302 w3c/303 w3c/307 \
    wepawet/300 wepawet/301 wepawet/302 wepawet/303 wepawet/307 \
    wepawet/frameset wepawet/iframe \
    wepawet/metarefresh wepawet/refreshheader \
    wepawet/onload wepawet/xhr \
"

# Make a directory with a numeric suffix to avoid overwriting.
function make_directory() {
	basename="$1"
	mkdir -p $(dirname "$basename")
	N=0
	dirname="$basename"
	while ! mkdir "$dirname"; do
		N=$((N+1))
		dirname="$basename".$N
	done
	echo "$dirname"
}

results_dir=$(make_directory "redirects-payload-results/$(date --iso)")

echo "Saving results in $results_dir."

MYIP=$(./myip.py -4)

for ossmethod in $OSS_METHOD_LIST; do
	echo $ossmethod
	python2.7 ./redirects-payload --oss=$ossmethod --return $MYIP 2>&1 | tee $results_dir/$(echo $ossmethod | sed -e 's/\//-/')
	# python2.7 ./redirects-payload --oss=manual/$(echo $ossmethod | awk -F/ '{print $2}') --return $MYIP 2>&1 | tee $results_dir/$(echo $ossmethod | sed -e 's/\//-/')
done
