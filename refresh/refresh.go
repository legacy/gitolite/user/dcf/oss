package main

import "errors"
import "fmt"
import "html"
import "net/http"
import "net/http/cgi"
import "os"
import "strconv"

func exitError(code int, msg string) {
	fmt.Printf("Status: %d\r\n", code)
	fmt.Printf("Content-Type: text/plain; charset=utf-8\r\n")
	fmt.Printf("\r\n")
	fmt.Printf("Error: %s.\n", msg)
	os.Exit(1)
}

func getFirstParam(req *http.Request, param string) (value string, err error) {
	values, ok := req.URL.Query()[param]
	if !ok || len(values) < 1 {
		err = errors.New("no \"%s\" parameter")
		return
	}

	return values[0], nil
}

func main() {
	req, err := cgi.Request()
	if err != nil {
		exitError(http.StatusInternalServerError, err.Error())
	}
	switch req.Method {
	case "GET":

	default:
		exitError(http.StatusMethodNotAllowed, "request method must be GET")
	}

	delay_str, err := getFirstParam(req, "delay")
	if err == nil {
		var delay, count int

		t, err := strconv.ParseInt(delay_str, 10, 0)
		if err != nil {
			exitError(400, fmt.Sprintf("can't parse delay: %s", err.Error()))
		}
		delay = int(t)

		count_str, err := getFirstParam(req, "count")
		if err == nil {
			t, err := strconv.ParseInt(count_str, 10, 0)
			if err != nil {
				exitError(400, fmt.Sprintf("can't parse count: %s", err.Error()))
			}
			count = int(t)
		} else {
			count = 0
		}

		kind, err := getFirstParam(req, "kind")
		if err == nil {
			if !(kind == "header" || kind == "meta") {
				exitError(400, fmt.Sprintf("don't know kind \"%s\"", kind))
			}
		} else {
			kind = "header"
		}
		refreshStr := fmt.Sprintf("%d; url=\"?kind=%s&count=%d&delay=%d\"", delay, kind, count + 1, delay)

		fmt.Printf("Status: 200\r\n")
		fmt.Printf("Content-Type: text/html; charset=utf-8\r\n")
		if kind == "header" {
			fmt.Printf("Refresh: %s\r\n", refreshStr)
		}
		fmt.Printf("Connection: close\r\n")
		fmt.Printf("\r\n")
		fmt.Printf(`<!DOCTYPE html>
<html>
<head>
`)
		if kind == "meta" {
			fmt.Printf("<meta http-equiv=refresh content=\"%s\">",
				html.EscapeString(refreshStr))
		}
		fmt.Printf(`<title>refresh counter</title>
</head>
<body>
<p>
You have nyaned for %d refreshes.
</p>
</body>
</html>
`, count)
	} else {
		fmt.Printf("Status: 200\r\n")
		fmt.Printf("Content-Type: text/html; charset=utf-8\r\n")
		fmt.Printf("Connection: close\r\n")
		fmt.Printf("\r\n")
		fmt.Printf(`<!DOCTYPE html>
<html>
<head>
<title>refresh counter</title>
</head>
<body>
<h3>refresh counter</h3>
<form>
<label>Delay: <input name="delay"></label><br>
<label><input type=radio name="kind" value="header" checked> Refresh header</label><br>
<label><input type=radio name="kind" value="meta"> meta refresh</label><br>
<input type=submit value="Go">
</form>
</body>
</html>
`)
	}
}
