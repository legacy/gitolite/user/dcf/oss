			# of redirects	max data (URL length/body size)

adsense/300		0		0		0.29
adsense/301		5		2047		1.17
adsense/302		5		2047		1.27
adsense/303		5		2047		1.35
adsense/307		5		2047		1.34
adsense/frameset	0		0		0.61
adsense/iframe		0		0		0.66
adsense/metarefresh	5		2047/32622	1.32
adsense/onload		0		0		0.52
adsense/refreshheader	5		1279/32662	1.24
adsense/xhr		0		0		0.66

drweb/300		>=249		8181		0.48
drweb/301		>=249		8181		0.53
drweb/302		>=249		8181		0.53
drweb/303		>=249		8181		0.54
drweb/307		>=249		8181		0.62
drweb/frameset		1		481764		0.61
drweb/iframe		1		454558		0.54
drweb/metarefresh	0		0		0.55
drweb/onload		0		0		0.59
drweb/refreshheader	0		0		0.40
drweb/xhr		0		0		0.53

facebook/300		-		-
facebook/301		25		-
facebook/302		25		-
facebook/303		25		-
facebook/307		-		-
facebook/frameset	0		-
facebook/iframe		-		-
facebook/metarefresh	-		-
facebook/onload		-		-
facebook/refreshheader	-		-
facebook/xhr		-		-

goo.gl/300		15		2047		n/a
goo.gl/301		15		2047		n/a
goo.gl/302		15		2047		n/a
goo.gl/303		15		2047		n/a
goo.gl/307		15		2047		n/a
goo.gl/frameset		9		2047		n/a
goo.gl/iframe		9		2047		n/a
goo.gl/metarefresh	30		2047		n/a
goo.gl/onload		30		524288		n/a
goo.gl/refreshheader	30		2047		n/a
goo.gl/xhr		15		524288		n/a

gomo/300		15		524288		3.48
gomo/301		15		524288		3.86
gomo/302		15		524288		3.49
gomo/303		15		524288		3.90
gomo/307		15		524288		3.65
gomo/frameset		201		524288		3.78
gomo/iframe		201		524288		4.36
gomo/metarefresh	>=249		524288		5.05
gomo/onload		>=249		524288		7.18
gomo/refreshheader	>=249		524288		4.03
gomo/xhr		>=249		524288		3.23

netrenderer/300		10		2083		1.15
netrenderer/301		10		2083		1.15
netrenderer/302		10		2083		1.11
netrenderer/303		10		2083		1.31
netrenderer/307		10		2083		1.13
netrenderer/frameset	~80 (~30 s)	2083		1.61
netrenderer/iframe	~80 (~30 s)	2083		1.18
netrenderer/metarefresh	1		2083		1.06
netrenderer/onload	0		0		1.03
netrenderer/refreshheader	1	2083		1.02
netrenderer/xhr		1		4096+		1.05

novirusthanks/300	10		134611		1.33
novirusthanks/301	10		134611		1.43
novirusthanks/302	10		134611		0.94
novirusthanks/303	10		134611		1.06
novirusthanks/307	10		134611		1.10
novirusthanks/frameset	0		0		0.66
novirusthanks/iframe	0		0		0.53
novirusthanks/metarefresh	0	0		0.52
novirusthanks/onload	0		0		0.53
novirusthanks/refreshheader	0	0		0.51
novirusthanks/xhr	0		0		0.71

pdfmyurl/300		0		0		0.89
pdfmyurl/301		>=249		524288		1.13
pdfmyurl/302		>=249		524288		2.08
pdfmyurl/303		>=249		524288		1.46
pdfmyurl/307		>=249		524288		1.63
pdfmyurl/frameset	201		524288		1.79
pdfmyurl/iframe		201		524288		1.70
pdfmyurl/metarefresh	>=249		524288		1.82
pdfmyurl/onload		>=249		524288		2.52
pdfmyurl/refreshheader	>=249		524288		2.88
pdfmyurl/xhr		>=249		524288		1.42

ref/300			0		0		0.39
ref/301			99		524288		0.45
ref/302			99		524288		0.69
ref/303			99		524288		0.52
ref/307			99		524288		0.38
ref/frameset		0		0		0.25
ref/iframe		0		0		0.27
ref/metarefresh		0		0		0.27
ref/onload		0		0		0.24
ref/refreshheader	0		0		0.37
ref/xhr			0		0		0.30

twitter/300		0		0		2.47
twitter/301		4/25		1447		2.32
twitter/302		4/25		1744		2.62
twitter/303		4/25/100	2142		2.76
twitter/307		4/25		1500		2.65
twitter/frameset	0		0		2.52
twitter/iframe		0		0		2.64
twitter/metarefresh	0		0		1.04
twitter/onload		0		0		2.69
twitter/refreshheader	0		0		2.59
twitter/xhr		0		0		2.86

virustotal/300		5/20		2047/10229	5.85
virustotal/301		5/20		2047/10229	11.02
virustotal/302		5/20		2047/10229	4.21
virustotal/303		5/20		2047/10229	9.24
virustotal/307		5/20		2047/10229	5.05
virustotal/frameset	0/9		0/524288	5.09
virustotal/iframe	0/9		0/524288	9.33
virustotal/metarefresh	0/~150 (~60 s)	0/524288	5.99
virustotal/onload	0		0		4.42
virustotal/refreshheader	0/154 (~60 s)	0/524288	4.52
virustotal/xhr		0		0		3.96

vurldissect/300		20		135763		22.56
vurldissect/301		20		135763		22.48
vurldissect/302		20		135763		22.44
vurldissect/303		20		135936		22.33
vurldissect/307		20		135763		22.39
vurldissect/frameset	0		0		22.39
vurldissect/iframe	0		0		22.30
vurldissect/metarefresh 0		0		22.34
vurldissect/onload	0		0		22.29
vurldissect/refreshheader	0	0		22.90
vurldissect/xhr		0		0		22.53

w3c/300			0		0		0.75
w3c/301			7		8181		0.68
w3c/302			7		8181		0.69
w3c/303			7		8181		0.89
w3c/307			7		8181		0.84
w3c/frameset		0		0		0.63
w3c/iframe		0		0		0.71
w3c/metarefresh		0		0		0.78
w3c/onload		0		0		0.69
w3c/refreshheader	0		0		0.81
w3c/xhr			0		0		0.65

chromium/300		0		0		n/a
chromium/301		20		262023		n/a
chromium/302		20		262035		n/a
chromium/303		20		262031		n/a
chromium/307		20		262022		n/a
chromium/frameset	>=249		819200		n/a
chromium/iframe		>=249		819200		n/a
chromium/metarefresh	>=249		819200		n/a
chromium/onload		>=249		819200		n/a
chromium/refreshheader	>=249		261951		n/a
chromium/xhr		>=249		819200		n/a

firefox/300		20		819200		n/a
firefox/301		20		819200		n/a
firefox/302		20		819200		n/a
firefox/303		20		819200		n/a
firefox/307		20		819200		n/a
firefox/frameset	9		819200		n/a
firefox/iframe		9		819200		n/a
firefox/metarefresh	>=249		819200		n/a
firefox/onload		>=249		819200		n/a
firefox/refreshheader	>=249		819200		n/a
firefox/xhr		>=249		819200		n/a

ie/300			120		4095		n/a
ie/301			120		524288		n/a
ie/302			120		524288		n/a
ie/303			120		524288		n/a
ie/307			120		524288		n/a
ie/frameset		>=249		4095		n/a
ie/iframe		>=249		4095		n/a
ie/metarefresh		>=249		524288		n/a
ie/onload		>=249		524288		n/a
ie/refreshheader	>=249		2098		n/a
ie/xhr			1		524288		n/a

safari/300		0		0		n/a
safari/301		16		262144 crash	n/a
safari/302		16		262144 crash	n/a
safari/303		16		262144 crash	n/a
safari/307		16		262144 crash	n/a
safari/frameset		>=249		524288		n/a
safari/iframe		>=249		524288		n/a
safari/metarefresh	>=249		262144 crash	n/a
safari/onload		>=249		524288		n/a
safari/refreshheader	>=249		262144 crash	n/a
safari/xhr		>=249		524288		n/a
